import 'package:flutter/material.dart';
import 'package:widget_transform/custom_overlay.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
          onTap: () {
            print('tap');
          },
          child: Center(
            child: Text(
              'okey',
              style: TextStyle(fontSize: 20),
            ),
          )),
      floatingActionButton: CustomOverlay(
        closedDecoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
        ),
        openColor: Colors.white,
        closedColor: Colors.blue,
        openChild: OpenPage(),
        duration: Duration(milliseconds: 400),
        closedChild: SizedBox(
          height: 60,
          width: 60,
          child: GestureDetector(
            onTap: () {
              CustomOverlay.toogle();
            },
            onVerticalDragStart: CustomOverlay.handleDragStart,
            onVerticalDragUpdate: CustomOverlay.handleDragUpdate,
            onVerticalDragEnd: CustomOverlay.handleDragEnd,
            child: Center(
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 30,
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class OpenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: () {
          CustomOverlay.toogle();
        },
        onVerticalDragStart: CustomOverlay.handleDragStart,
        onVerticalDragUpdate: CustomOverlay.handleDragUpdate,
        onVerticalDragEnd: CustomOverlay.handleDragEnd,
        child: Text(
          'Back',
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }
}
