import 'dart:math';
import 'dart:ui';
import 'package:flutter/material.dart';

class CustomOverlay extends StatefulWidget {
  final Widget openChild;
  final Widget closedChild;
  final BoxDecoration openDecoration;
  final BoxDecoration closedDecoration;
  final Color openColor;
  final Color closedColor;
  final Duration duration;

  CustomOverlay({
    @required this.openChild,
    @required this.closedChild,
    this.openDecoration = const BoxDecoration(
      borderRadius: BorderRadius.zero,
    ),
    this.closedDecoration,
    this.openColor = Colors.white,
    this.closedColor = Colors.white,
    this.duration = const Duration(milliseconds: 300),
  });

  static OverlayEntry overlayEntry;
  static OverlayState overlay;
  static AnimationController controller;
  static double maxHeight;
  static BuildContext _context;

  static void generateContext(BuildContext context) {
    _context = context;
    maxHeight = MediaQuery.of(_context).size.height;
  }

  static void toogle() {
    final bool isOpen = controller.status == AnimationStatus.completed;
    if (isOpen) {
      controller.reverse().then((value) {
        overlayEntry.remove();
      });
    } else {
      overlay.insert(overlayEntry);
      controller.forward();
    }
  }

  static void handleDragStart(DragStartDetails details) {
    if (details.globalPosition > details.localPosition) {
      overlay.insert(overlayEntry);
    }
  }

  static void handleDragUpdate(DragUpdateDetails details) {
    controller.value -= details.primaryDelta / maxHeight;
  }

  static void handleDragEnd(DragEndDetails details) {
    if (controller.isAnimating ||
        controller.status == AnimationStatus.completed) return;

    final double flingVelocity =
        details.velocity.pixelsPerSecond.dy / maxHeight;
    if (flingVelocity < 0.0)
      controller.fling(velocity: max(2.0, -flingVelocity));
    else if (flingVelocity > 0.0)
      controller.fling(velocity: min(-2.0, -flingVelocity)).then((value) {
        overlayEntry.remove();
      });
    else {
      if (controller.value < 0.5) {
        controller.fling(velocity: -2).then((value) {
          overlayEntry.remove();
        });
      } else {
        controller.fling(velocity: 2);
      }
    }
  }

  @override
  _CustomOverlayState createState() => _CustomOverlayState();
}

class _CustomOverlayState extends State<CustomOverlay>
    with TickerProviderStateMixin {
  GlobalKey _key = GlobalKey();
  double topWidget;
  double leftWidget;
  bool isAnimation;
  Size widgetSize;

  Animation<double> opacity;
  Animation<double> left;
  Animation<double> right;
  Animation<double> top;
  Animation<double> bottom;

  void getPosition() {
    RenderBox box = _key.currentContext.findRenderObject();
    Offset position = box.localToGlobal(Offset.zero);
    setState(() {
      topWidget = position.dy;
      leftWidget = position.dx;
      widgetSize = box.size;
    });
  }

  OverlayEntry _createOverlayEntry() {
    return OverlayEntry(
        maintainState: true,
        builder: (context) {
          return Scaffold(
            backgroundColor: Colors.transparent,
            body: AnimatedBuilder(
              animation: CustomOverlay.controller,
              builder: (context, child) {
                return Stack(
                  children: [
                    Container(
                      height: double.infinity,
                      width: double.infinity,
                      color: Colors.black.withOpacity(opacity.value),
                    ),
                    Positioned(
                      left: left.value,
                      top: top.value,
                      bottom: bottom.value,
                      right: right.value,
                      child: GestureDetector(
                        onVerticalDragStart: CustomOverlay.handleDragStart,
                        onVerticalDragUpdate: CustomOverlay.handleDragUpdate,
                        onVerticalDragEnd: CustomOverlay.handleDragEnd,
                        child: Container(
                          decoration: BoxDecoration.lerp(
                              widget.closedDecoration
                                  .copyWith(color: widget.closedColor),
                              widget.openDecoration
                                  .copyWith(color: widget.openColor),
                              CustomOverlay.controller.value),
                          child: CustomOverlay.controller.value <= 0.5
                              ? widget.closedChild
                              : widget.openChild,
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    CustomOverlay.controller =
        AnimationController(vsync: this, duration: widget.duration);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      CustomOverlay.overlay = Overlay.of(context);
      getPosition();
      CustomOverlay.overlayEntry = _createOverlayEntry();
      opacity = Tween(
        begin: 0.0,
        end: 0.5,
      ).animate(CurvedAnimation(
        parent: CustomOverlay.controller,
        curve: Curves.easeOutQuart,
      ));
      left = Tween(
        begin: leftWidget,
        end: 0.0,
      ).animate(CurvedAnimation(
        parent: CustomOverlay.controller,
        curve: Curves.easeOutQuart,
      ));
      right = Tween(
        begin:
            MediaQuery.of(context).size.width - (leftWidget + widgetSize.width),
        end: 0.0,
      ).animate(CurvedAnimation(
        parent: CustomOverlay.controller,
        curve: Curves.easeOutQuart,
      ));
      top = Tween(
        begin: topWidget,
        end: 0.0,
      ).animate(CurvedAnimation(
        parent: CustomOverlay.controller,
        curve: Curves.easeOutQuart,
      ));
      bottom = Tween(
        begin: MediaQuery.of(context).size.height -
            (topWidget + widgetSize.height),
        end: 0.0,
      ).animate(CurvedAnimation(
        parent: CustomOverlay.controller,
        curve: Curves.easeOutQuart,
      ));
    });
  }

  @override
  void dispose() {
    CustomOverlay.controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    CustomOverlay.generateContext(context);
    return Visibility(
      visible: true,
      child: Container(
        key: _key,
        decoration: widget.closedDecoration.copyWith(color: widget.closedColor),
        child: widget.closedChild,
      ),
    );
  }
}
